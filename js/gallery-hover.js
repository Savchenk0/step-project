let blockGallery = [...document.querySelectorAll(".work-img__wrapper")]

const imgHoverClass = {
    graphicDesign:"graphic design",
    webDesign:"web design",
    landingPages:"landing pages",
    wordpress:"wordpress",
}
const imgHoverPhrase = {
    graphicDesign:"Awesome design",
    webDesign:"Creative design",
    landingPages:"Variety of patterns",
    wordpress:"functional",
}

function  insertHover(){
    blockGallery.forEach(function(element){
        let imgClass = element.firstChild.className
        element.insertAdjacentHTML('beforeend',`
        <div class="work-img__hover">
    <img src="../images/work-hover-img.png" alt="" class="img-hover__logo">
    <p class="custom-phrase">${imgHoverPhrase[`${imgClass}`]}</p>
    <p class="menu-classname">${imgHoverClass[`${imgClass}`]}</p>
    </div>`)
        
    })
}
insertHover()








/* <div class="work-img__hover">
<img src="../images/work-hover-img.png" alt="" class="img-hover__logo">
<p class="custom-phrase">creative design</p>
<p class="menu-classname">Web Design</p>
</div> */