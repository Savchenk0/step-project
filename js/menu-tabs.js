
document.addEventListener("click", function(event){
    let servicesBtnArray = [...document.getElementsByClassName("services-btn")];
    let servicesContentArray = [...document.getElementsByClassName("services-content")];
    let eventNumber = servicesBtnArray.indexOf(event.target)
    if (servicesBtnArray.includes(event.target)){
        let currentActive = document.querySelector('.active-btn')
        if (!event.target.classList.contains("active-btn")){
            if (currentActive){
                let currentNumber = servicesBtnArray.indexOf(currentActive);
                servicesContentArray[currentNumber].style.display = "none"
                currentActive.classList.remove('active-btn')
            }
            event.target.classList.add('active-btn')
            servicesContentArray[eventNumber].style.display = "flex"
        }
    }
})

