let blogpostDates = [...document.querySelectorAll(".gallery-date")]

blogpostDates.forEach(function(element){
    element.insertAdjacentHTML("beforeend",`<span class="date-text">12<br>Feb</span>`)
})


$(document).ready(function(){
    $('.slider').slick({
        dots: true,
        customPaging: function(slick,index) {
            let targetImage = slick.$slides.eq(index).find('img').attr('src');
            return '<img class="slick-dot-img" src=" ' + targetImage + ' "/>';
        },
        prevArrow: '<span class="slick-arrow slick-prev"><img src="../images/slide-left.svg" alt="arrow"></span>',
        nextArrow: '<span class="slick-arrow slick-next"><img src="../images/right-slide.svg" alt="arrow"></span>',
    })
});
