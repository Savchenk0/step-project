document.addEventListener("click",function(event){
    let currentActive = document.querySelector('.work-menu__btn__active')
    if (event.target.classList.contains("work-menu__btn")){
        if (currentActive){
            currentActive.classList.remove('work-menu__btn__active')
            currentActive.classList.remove("active-filter")
        }
        event.target.classList.add('work-menu__btn__active')
        event.target.classList.add('active-filter')

    }
})


