let galleryImages = [...document.querySelectorAll(".work-img__wrapper")]
const workBtnArray = [...document.querySelectorAll(".work-menu__btn")]


const graphic1 = [...document.querySelectorAll(".graphicDesign")]
const webDesign2 = [...document.querySelectorAll(".webDesign")]
const landingPages3 = [...document.querySelectorAll(".landingPages")]
const wordpress4 = [...document.querySelectorAll(".wordpress")]

workBtnArray.forEach(function(tab,index){
    tab.addEventListener("click",function(event){
       if (index === 0){
           galleryImages.forEach(function(element){
               element.style.display = "block"
           })
       }
       if (index === 1){
        galleryImages.forEach(function(element){
            element.style.display = "none"
        })
        graphic1.forEach(function(element){
            element.parentElement.style.display = "block"
        })
       }
       if (index === 2){
        galleryImages.forEach(function(element){
            element.style.display = "none"
        })
        webDesign2.forEach(function(element){
            element.parentElement.style.display = "block"
        })
       }
       if (index === 3){
        galleryImages.forEach(function(element){
            element.style.display = "none"
        })
        landingPages3.forEach(function(element){
            element.parentElement.style.display = "block"
        })
       }
       if (index === 4){
        galleryImages.forEach(function(element){
            element.style.display = "none"
        })
        wordpress4.forEach(function(element){
            element.parentElement.style.display = "block"
        })
       }
    })
})