const workGallery = [...document.querySelectorAll(".work-gallery__wrapper img")]
let index = 1;
workGallery.forEach((element,index) => {
    if (index+1 > 12){
    element.style.display = "none"
    }
});

document.addEventListener("click",
function(event){
    let loadBtn = document.querySelector(".load-more__btn")
    if (event.target.classList.contains("load-more__btn")){
        workGallery.forEach((element,index) => {
            if (index+1 > 12){
            element.style.removeProperty("display")
            }
    })
    loadBtn.remove()
}})